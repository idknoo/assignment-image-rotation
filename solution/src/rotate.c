#include "image.h"

struct pixel image_get_pixel(struct image const* img, uint32_t x, uint32_t y) {
    return *(img->data + y * img->width + x);
}

static int image_check_coords(struct image const* img, uint32_t x, uint32_t y) {
    return x >= 0 && x <= img->width && y >= 0 && y <= img->height;
}

int image_set_pixel(struct image *img, struct pixel const p, uint32_t x, uint32_t y) {
    if(!image_check_coords(img, x, y))
        return 0;
    *(img->data + y * img->width + x) = p;
    return 1;
}

struct image rotate_90(struct image source) {
    struct image rotated = {0};
    create_image(&rotated, source.height, source.width);
    for(uint32_t i = 0; i < source.height; i++)
        for(uint32_t j = 0; j < source.width; j++) {
            image_set_pixel(&rotated, image_get_pixel(&source, j, source.height - i - 1), i, j);
        }

    return rotated;
}
