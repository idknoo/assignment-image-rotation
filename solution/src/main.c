#include "bmp.h"
#include "actions.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

const char* read_status[] = {
    [READ_GOOD] = "Data read successfully\n",
    [READ_FILE_GOOD] = "File read successfully\n",
    [READ_HEADER_ERROR] = "Failed to read header\n",
    [READ_FILE_ERROR] = "Failed to read file\n"
};  

const char* write_status[] = {
    [WRITE_GOOD]= "Data was written\n",
    [WRITE_FILE_GOOD] = "Data was written to the file\n",
    [WRITE_ERROR] = "Data was not recorded\n",
    [WRITE_FILE_ERROR] = "Data was not written to the file\n" 
};

void write_to_stdout(const char* status) {
    fprintf(stdout, "%s", status);
}

void write_to_stderr(const char* status) {
    fprintf(stderr, "%s", status);
}


int main( int argc, char** argv ) {
    (void) argc; (void) argv; 

    if (argc != 3) {
        fprintf(stderr, "%s", "Incorrect input");
        return 1;
    }
    struct image source = {0};
    struct image result = {0};
    enum read_status rd_status = read_image(argv[1], &source);
    
    if (rd_status > 1) {
        write_to_stdout(read_status[rd_status]);
        return 1;
    } else 
        write_to_stdout(read_status[rd_status]);

    result = rotate_90(source);
    enum write_status wr_status = write_image(argv[2], &result);
    if (wr_status > 1) {
        write_to_stderr(write_status[wr_status]);
        return 1;
    } else 
        write_to_stderr(write_status[wr_status]);

    delete(result);
    delete(source);  
    return 0;
}
