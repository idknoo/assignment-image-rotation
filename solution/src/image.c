#include "image.h"

struct image newimg(size_t width, size_t height) {
    struct image img = {0};
    img.width = width;
    img.height = height;
    return img;
}

void delete(struct image img) {
   free(img.data);
}

void create_image(struct image* image,uint32_t width,uint32_t height){
    image->height = height;
    image->width = width;
    image->data = malloc(sizeof(struct pixel) * width * height);
}
