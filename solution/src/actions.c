#include "actions.h"

enum read_status read_image(char* const filename, struct image* const img) {
    FILE *FILE_NAME;
    FILE_NAME = fopen(filename,"rb");
    if (!FILE_NAME) return READ_FILE_ERROR;

    enum read_status rd_status = from_bmp(FILE_NAME, img);
    if (rd_status)
        return rd_status;

    enum close_status close_status = file_close(&FILE_NAME);
    if (close_status != CLOSE_SUCCESS) {
        return READ_FILE_ERROR;
    }
    return READ_FILE_GOOD;
}

enum write_status write_image(char* const filename, struct image* const img){
    FILE *FILE_NAME;
    FILE_NAME = fopen(filename,"wb");
    if (!FILE_NAME) return WRITE_FILE_ERROR;

    enum write_status wr_status = to_bmp(FILE_NAME, img);
    if(wr_status) 
        return wr_status;

    enum close_status close_status = file_close(&FILE_NAME);
    if (close_status != CLOSE_SUCCESS) {
        return WRITE_FILE_ERROR;
    }
    return WRITE_FILE_GOOD;
}

enum close_status file_close(FILE** file) {
    if (*file) {
        if (fclose(*file) == 0) {
            return CLOSE_SUCCESS;
        } 
        else {
            return CLOSE_UNKNOWN_ERROR;
        }
    }

    return CLOSE_FILE_NOT_EXIST;
}


