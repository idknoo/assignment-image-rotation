#include "bmp.h"

#pragma pack(push, 1)
#ifndef BMP
#define BMP
struct bmp_header{
    uint16_t bfType;
    uint32_t bFileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#endif
#pragma pack(pop)

static uint32_t calculated_padding(uint32_t width);

static enum read_status read_data(FILE *in, struct image img) {
    struct pixel *pixels = img.data;
    const size_t width = img.width;
    const size_t height = img.height;

    const uint32_t padding = calculated_padding(img.width);
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + width * i, sizeof(struct pixel), width, in) != width || (fseek(in, padding, SEEK_CUR))) {
            free(pixels);
            return READ_FILE_ERROR;
        }
    }
    return READ_GOOD;
}

static enum write_status write_data(FILE *out, struct image const img) {
    const struct pixel *new_pix = img.data;
    const size_t width = img.width;
    const size_t height = img.height;

    const uint32_t padding = calculated_padding(img.width);
    for (size_t i = 0; i < height; i++) {
        if (fwrite(new_pix, sizeof(struct pixel), width, out) != width || (fwrite(new_pix, 1, padding, out) != padding))
            return WRITE_ERROR;
        new_pix = new_pix + (size_t) width;
    }
    return WRITE_GOOD;
}

static enum read_status read_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in))
        return READ_GOOD;
    else
        return READ_HEADER_ERROR;
}

struct bmp_header header_from_image(const struct image *img) {
    const uint32_t padding = calculated_padding(img->width);
    size_t size = sizeof(struct pixel) * img->width * img->height + padding * img->height;

    struct bmp_header header = {0};
    header.bfType = TYPE;
    header.bFileSize = HEADER_SIZE + size;
    header.bOffBits = HEADER_SIZE;
    header.biSize = DIB_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = BPP;
    header.biCompression = COMP;
    header.biSizeImage = size;
    header.biXPelsPerMeter = X_PPM;
    header.biYPelsPerMeter = Y_PPM;
    header.biClrUsed = NUM_COLORS;
    header.biClrImportant = IMP_COLORS;
    return header;
}

static enum write_status write_header(FILE *out, struct image const *img) {

    struct bmp_header header = header_from_image(img);

    if (!(fwrite(&header, sizeof(struct bmp_header), 1, out)))
        return WRITE_ERROR;
    else
        return WRITE_GOOD;
}

static uint32_t calculated_padding(uint32_t width) {
    uint32_t padding;
    if (!(width % 4))
        padding = 0;
    else
        padding = (width * 3 / 4 + 1) * 4 - width * 3;
    return padding;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if(out == NULL || img == NULL)
        return WRITE_NULL_PTR;
    if (write_header(out, img))
        return WRITE_ERROR;
    return write_data(out, *img);
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    enum read_status status = read_header(in, header);

    if (status)
        return status;

    create_image(img, header->biWidth, header->biHeight);

    status = read_data(in, *img);
    free(header);

    return status;
}

