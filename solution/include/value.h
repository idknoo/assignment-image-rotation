#ifndef VALUE
#define VALUE

enum read_status  {
  READ_GOOD = 0,
  READ_FILE_GOOD, 
  READ_HEADER_ERROR,
  READ_FILE_ERROR
};

enum  write_status  {
  WRITE_GOOD = 0,
  WRITE_FILE_GOOD,
  WRITE_ERROR,
  WRITE_FILE_ERROR,
  WRITE_NULL_PTR
};

enum close_status {
    CLOSE_FILE_NOT_EXIST,
    CLOSE_UNKNOWN_ERROR,
    CLOSE_SUCCESS
}; 

#endif
