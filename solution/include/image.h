#include <stdint.h>
#include <stdlib.h>

#ifndef IMAGE_STRUCTS
#define IMAGE_STRUCTS
struct pixel { uint8_t b, g, r; } __attribute__((packed));

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif

struct image newimg(size_t width, size_t height);
void delete(struct image img);
void create_image(struct image* image,uint32_t width,uint32_t height);
