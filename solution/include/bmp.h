#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "image.h"
#include "value.h"

#define TYPE 19778
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0
#define HEADER_SIZE 54


enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );
